﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


///<summary>
///Fake condition which is always true. It has no side effect. Can be used for testing and code debuging.
///</summary>
public sealed class AlwaysTrueCondition : ICondition
{
    #region Execute

    ///<summary>
    ///Checks if new box can be added to the current matrix against the condition's definition.
    ///</summary>
    public bool Execute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new Exception("Matrix cannot be null");

        if (box == null)
            throw new Exception("Box cannot be null");

        return true;
    }

    #endregion
}
