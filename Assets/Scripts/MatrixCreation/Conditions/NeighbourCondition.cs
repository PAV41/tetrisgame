﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Condition which is checking if two neighbours are not the same type.
///</summary>
public class NeighbourCondition : ICondition {

    #region Execute

    ///<summary>
    ///Checks if new box can be added to the current matrix against the condition's definition.
    ///</summary>
    public bool Execute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new Exception("Matrix cannot be null");

        if (box == null)
            throw new Exception("Box cannot be null");

        var neightBours = matrix.GetBoxesNeighbours(box);
        if (neightBours.Any())
        {
            foreach (var boxNeightBour in neightBours)
            {
                if (matrix.AreBoxesTheSameType(boxNeightBour, box))
                    return false;
            }
        }
        return true;
    }

    #endregion
}
