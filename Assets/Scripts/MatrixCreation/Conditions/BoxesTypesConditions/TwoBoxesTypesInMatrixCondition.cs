﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


///<summary>
///Condition which is allowing only two distinct box types inside whole matrix.
///</summary>
public sealed class TwoBoxesTypesInMatrixCondition : BoxesTypesInMatrixConditionBase
{
    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct box types. In this case 2.
    ///</summary>
    protected override int GetAllowedBoxesTypesCount()
    {
        return 2;
    }

    #endregion
}
