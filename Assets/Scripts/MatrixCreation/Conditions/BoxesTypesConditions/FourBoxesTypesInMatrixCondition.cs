﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Condition which is allowing only four distinct box types inside whole matrix.
///</summary>
public sealed class FourBoxesTypesInMatrixCondition : BoxesTypesInMatrixConditionBase
{

    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct box types. In this case 4.
    ///</summary>
    protected override int GetAllowedBoxesTypesCount()
    {
        return 4;
    }

    #endregion

}
