﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Condition which is allowing only five distinct box types inside whole matrix.
///</summary>
public sealed class FiveBoxesTypesInMatrixCondition : BoxesTypesInMatrixConditionBase {

    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct box types. In this case 5.
    ///</summary>
    protected override int GetAllowedBoxesTypesCount()
    {
        return 5;
    }

    #endregion

}
