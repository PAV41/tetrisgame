﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Condition which is allowing only six distinct box types inside whole matrix.
///</summary>
public sealed class SixBoxesTypesInMatrixCondition : BoxesTypesInMatrixConditionBase
{
    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct box types. In this case 6.
    ///</summary>
    protected override int GetAllowedBoxesTypesCount()
    {
        return 6;
    }

    #endregion
}
