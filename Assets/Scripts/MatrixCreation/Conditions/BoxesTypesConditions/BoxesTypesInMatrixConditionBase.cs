﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Abstract condition which is allowing only N distinct box types inside whole matrix.
///</summary>
public abstract class BoxesTypesInMatrixConditionBase : ICondition {

    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct boxes types.
    ///</summary>
    protected abstract int GetAllowedBoxesTypesCount();

    #endregion

    #region Execute

    ///<summary>
    ///Checks if new box can be added to the current matrix against the condition's definition.
    ///</summary>
    public bool Execute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new Exception("Matrix cannot be null");

        if (box == null)
            throw new Exception("Box cannot be null");

        var boxes = matrix.GetAllBoxes();
        if (boxes.Any())
        {
            var distinctTypesList = from b in boxes where b.IsStandard
                                    group b by b.GetType() into groupedBoxes
                                    select groupedBoxes.Key;

            if (distinctTypesList.Any())
            {
                if (distinctTypesList.Count() >= GetAllowedBoxesTypesCount())
                {
                    if (!distinctTypesList.Contains(box.GetType()))
                        return false;
                }
            }
        }
        return true;
    }

    #endregion
}
