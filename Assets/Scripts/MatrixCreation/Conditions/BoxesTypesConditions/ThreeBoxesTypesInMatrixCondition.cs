﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Condition which is allowing only three distinct box types inside whole matrix.
///</summary>
public sealed class ThreeBoxesTypesInMatrixCondition : BoxesTypesInMatrixConditionBase
{
    #region GetAllowedBoxesCount

    ///<summary>
    ///Returns allowed number of distinct box types. In this case 3.
    ///</summary>
    protected override int GetAllowedBoxesTypesCount()
    {
        return 3;
    }

    #endregion
}
