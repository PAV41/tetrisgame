﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICondition {

    ///<summary>
    ///Checks if new box can be added to the current matrix against the condition's definition.
    ///</summary>
    bool Execute(IMatrix matrix, IBox box);
}
