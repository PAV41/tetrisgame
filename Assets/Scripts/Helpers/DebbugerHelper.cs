﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

///<summary>
///Static class whitch contains useful functions during debbuging. Please be aware that below functions shouldn't be use in release build.
///</summary>
public static class DebbugerHelper
{
    #region CaptureScreenShoot

    ///<summary>
    ///Creates png file  whitch contains current game state.
    ///</summary>
    public static void CaptureScreenShoot()
    {
        string directoryPath = Const.PhysicalDirectories.ScreenShootsDirectory;
        if (string.IsNullOrEmpty(directoryPath))
            throw new Exception("Cannot capture screen. Screens directory has not been defined. Please define it first.");

        if (!Directory.Exists(directoryPath))
            Directory.CreateDirectory(directoryPath);

        string guid = Guid.NewGuid().ToString();
        var uniqueFileName = string.Format("{0}.png", guid);
        string fullImgPath = Path.Combine(directoryPath, uniqueFileName);
        ScreenCapture.CaptureScreenshot(fullImgPath);
    }

    #endregion
}
