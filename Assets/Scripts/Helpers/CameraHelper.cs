﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Helper static class related to camera parameters.
///</summary>
public static class CameraHelper {

    ///<summary>
    ///Returns camera height in Unity's units.
    ///</summary>
    public static int GetCameraHeight(int margin = 1)
    {
        Camera camera = Camera.main;
        if (camera == null)
            throw new System.Exception("Main camera doesn't exists");

        return (int)(2f * camera.orthographicSize - margin);
    }

    ///<summary>
    ///Returns camera width in Unity's units.
    ///</summary>
    public static int GetCameraWidth(int margin = 1)
    {
        Camera camera = Camera.main;
        if (camera == null)
            throw new System.Exception("Main camera doesn't exists");

        float height = (2f * camera.orthographicSize - margin);

        return (int)(height * camera.aspect - margin);
    }

}
