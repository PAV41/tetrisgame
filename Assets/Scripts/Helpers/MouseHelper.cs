﻿using UnityEngine;
using System.Collections;
using System;

///<summary>
///Helper static class related to mouse inputs.
///</summary>
public static class MouseHelper {

	///<summary>
	///Transforms real mouse position in pixels to Unity's units. 
	///</summary>
    public static Vector3 GetMouseLogicalPosition()
    {
        Vector3 result = new Vector3();
        var logicalPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if(logicalPos != null)
        {
            int x = (int)Math.Round(logicalPos.x, 0, MidpointRounding.AwayFromZero);
            int y = (int)Math.Round(logicalPos.y, 0, MidpointRounding.AwayFromZero);
            int z = 0;

            result.x = x;
            result.y = y;
            result.z = z;
        }
        return result;
    }

}
