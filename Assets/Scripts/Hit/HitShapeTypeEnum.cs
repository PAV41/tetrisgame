﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitShapeTypeEnum
{
    Unrecognized,
    Horizontal,
    Vertical,
    LLetter
}
