﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHit
{
    ///<summary>
    ///Checks if hit can be executed. Initializes internal list with boxes.
    ///</summary>
    bool CanExecute(IMatrix matrix, IBox box);

    ///<summary>
    ///Returns hit's priority;
    ///</summary>
    int GetPriority();

    ///<summary>
    ///Executes hit.
    ///</summary>
    void Execute(IMatrix matrix);

    ///<summary>
    ///Returs all boxes which are member of current hit.
    ///</summary>
    IList<IBox> GetBoxesInHit();

    ///<summary>
    ///Checks hit's shape. Currently only two shapes are recognized: horizontal or vertical.
    ///</summary>
    HitShapeTypeEnum GetHitShapeType();

    ///<summary>
    ///Returns number of points assigned to the hit.
    ///</summary>
    int GetPoints();
}

