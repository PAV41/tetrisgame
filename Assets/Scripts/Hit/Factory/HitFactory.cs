﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Helper static class (factory) responsible for hits instances creation.
///</summary>
public static class HitFactory
{
    ///<summary>
    ///Creates various hits instances.
    ///</summary>
    public static IHit CreateHit(HitTypeEnum type)
    {
        IHit hit = null;
        switch (type)
        {
            case HitTypeEnum.TwoElementsHit:
                hit = new TwoElementsHit();
                break;
            case HitTypeEnum.ThreeElementsHit:
                hit = new ThreeElementsHit();
                break;
            case HitTypeEnum.FourElementsHit:
                hit = new FourElementsHit();
                break;
            case HitTypeEnum.FiveElementsHit:
                hit = new FiveElementsHit();
                break;
            case HitTypeEnum.LHit:
                hit = new LHit();
                break;
            default:
                break;
        }
        return hit;
    }
}
