﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Represents hit which looks like capital L letter. 
///</summary>
public class LHit : HitBase
{
    #region CanExecute

    public override bool CanExecute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (box == null)
            throw new System.Exception("Box cannot be null");

        _boxesInHit = new List<IBox>();
        _initialBox = null;

        IList<IBox> boxesInColumnRight = new List<IBox>();
        IList<IBox> boxesInRowAbove = new List<IBox>();

        var boxesInColumn = matrix.GetBoxesOfSameTypeInAColumn(box);
        if (boxesInColumn.Any())
            boxesInColumnRight = boxesInColumn.Where(b => b.LogicalY > box.LogicalY).ToList();

        var boxesInRow = matrix.GetBoxesOfSameTypeInARow(box);
        if(boxesInRow.Any())
            boxesInRowAbove = boxesInRow.Where(b => b.LogicalX > box.LogicalX).ToList();

        if(boxesInColumnRight.Any() && boxesInRowAbove.Any())
        {
            if (boxesInColumnRight.Count >= 2 && boxesInRowAbove.Count >= 2)
            {
                foreach (var boxToAdd in boxesInColumnRight)
                    _boxesInHit.Add(boxToAdd);

                foreach (var boxToAdd in boxesInRowAbove)
                    _boxesInHit.Add(boxToAdd);

                _boxesInHit.Add(box);

                _initialBox = ChooiceInitialBox(_boxesInHit, box);
                return true;
            }

        }
        return false;
    }

    #endregion

    #region Execute

    public override void Execute(IMatrix matrix)
    {
        Debug.Log(string.Format("Executing hit on matrix => {0}", ToString()));

        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        var specialBox = TryGetTheMostRelevantSpecialBox();
        if (specialBox != null)
        {
            ApplySpecialBoxProperty(matrix, specialBox);
        }
        else
        {
            foreach (var boxInHit in _boxesInHit)
                matrix.UnregisterBox(boxInHit);

            IBox newSpecialBox = null;
            if (_initialBox.GetType() == typeof(BlueBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.BlueCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);
            else if (_initialBox.GetType() == typeof(RedBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.RedCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);
            else if (_initialBox.GetType() == typeof(GreenBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.GreenCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);
            else if (_initialBox.GetType() == typeof(GrayBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.GrayCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);
            else if (_initialBox.GetType() == typeof(VioletBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.VioletCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);
            else if (_initialBox.GetType() == typeof(YellowBox))
                newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.YellowCanDestroy3x3AreaBox, _initialBox.LogicalX, _initialBox.LogicalY);

            if (newSpecialBox == null)
                throw new System.Exception(string.Format("Cannot execute hit: {0}. Special box type has not been found for type: {1}", this.ToString(), _initialBox.GetType().ToString()));

            matrix.RegisterBox(newSpecialBox);
        }

        matrix.CollapseMatrix();
        matrix.FillUpMatrixWithNewBoxes();
    }

    #endregion

    #region GetMinBoxesInHit

    protected override int GetMinBoxesInHit()
    {
        return 5;
    }

    #endregion

    #region GetPriority

    public override int GetPriority()
    {
        return 4;
    }

    #endregion

    #region GetPoints

    public override int GetPoints()
    {
        return 100;
    }

    #endregion
}
