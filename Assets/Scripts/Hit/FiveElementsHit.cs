﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Represents hit which contains five the same boxes. 
///</summary>
public class FiveElementsHit : HitBase
{
    #region CanExecute

    ///<summary>
    ///Checks if five elements hit exists.
    ///</summary>
    public override bool CanExecute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (box == null)
            throw new System.Exception("Box cannot be null");

        _boxesInHit = new List<IBox>();
        _initialBox = null;

        var boxesInColumn = matrix.GetBoxesOfSameTypeInAColumn(box);
        if (boxesInColumn.Any() && boxesInColumn.Count >= GetMinBoxesInHit())
        {
            _boxesInHit = boxesInColumn;
            _initialBox = ChooiceInitialBox(boxesInColumn, box);
            return true;
        }

        var boxesInRow = matrix.GetBoxesOfSameTypeInARow(box);
        if (boxesInRow.Any() && boxesInRow.Count >= GetMinBoxesInHit())
        {
            _boxesInHit = boxesInRow;
            _initialBox = ChooiceInitialBox(boxesInRow, box);
            return true;
        }

        return false;
    }

    #endregion

    #region Execute

    ///<summary>
    ///Executes five elements hit.
    ///</summary>
    public override void Execute(IMatrix matrix)
    {
        Debug.Log(string.Format("Executing hit on matrix => {0}", this.ToString()));

        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        foreach (var boxInHit in _boxesInHit)
            matrix.UnregisterBox(boxInHit);

        IBox newSpecialBox = BoxFactory.CreateBox(BoxTypeEnum.CanDestroyAnyBoxTypeBox, _initialBox.LogicalX, _initialBox.LogicalY);

        if (newSpecialBox == null)
            throw new System.Exception(string.Format("Cannot execute hit: {0}. Special box type has not been found for type: {1}", this.ToString(), _initialBox.GetType().ToString()));

        matrix.RegisterBox(newSpecialBox);

        matrix.CollapseMatrix();
        matrix.FillUpMatrixWithNewBoxes();
    }

    #endregion

    #region GetMinBoxesInHit

    ///<summary>
    ///Returns minimum number of required boxes in the hit. In this case five.
    ///</summary>
    protected override int GetMinBoxesInHit()
    {
        return 5;
    }

    #endregion

    #region GetPriority

    public override int GetPriority()
    {
        return 5;
    }

    #endregion

    #region GetPoints

    ///<summary>
    ///Returns number of points assigned to the hit. In this case 50 points;
    ///</summary>
    public override int GetPoints()
    {
        return 50;
    }

    #endregion
}
