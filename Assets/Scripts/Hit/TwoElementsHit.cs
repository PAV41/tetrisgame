﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

///<summary>
///Represents hit which contains two the same boxes (one of them must have CanDestroyAnyBoxType property). 
///</summary>
public class TwoElementsHit : HitBase
{
    #region CanExecute

    ///<summary>
    ///Checks if three elements hit exists.
    ///</summary>
    public override bool CanExecute(IMatrix matrix, IBox box)
    {
        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (box == null)
            throw new System.Exception("Box cannot be null");

        _boxesInHit = new List<IBox>();
        _initialBox = null;

        var selectedBoxes = matrix.GetSelectedBoxes();
        if (selectedBoxes.Any() && selectedBoxes.Count == 2 && selectedBoxes.Contains(box) && box.IsStandard)
        {
            var specialBox = selectedBoxes.FirstOrDefault(b => b.HasProperty(BoxPropertyEnum.CanDestroyAnyBoxType));
            if (specialBox != null)
            {
                _boxesInHit = selectedBoxes;
                _initialBox = box;
                return true;
            }
        }

        return false;
    }

    #endregion

    #region Execute

    ///<summary>
    ///Executes two elements hit.
    ///</summary>
    public override void Execute(IMatrix matrix)
    {
        Debug.Log(string.Format("Executing hit on matrix => {0}", ToString()));

        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        var specialBox = TryGetTheMostRelevantSpecialBox();
        if (specialBox == null)
            throw new Exception(string.Format("Cannot execute hit: {0}. It requires special box.", this.GetType()));

        if (!specialBox.HasProperty(BoxPropertyEnum.CanDestroyAnyBoxType))
            throw new Exception(string.Format("Cannot execute hit: {0}. Expected box with property: {1}.", this.GetType(), BoxPropertyEnum.CanDestroyAnyBoxType));

        ApplySpecialBoxProperty(matrix, specialBox);
        matrix.UnregisterBox(specialBox);

        matrix.CollapseMatrix();
        matrix.FillUpMatrixWithNewBoxes();
    }

    #endregion

    #region GetMinBoxesInHit

    ///<summary>
    ///Returns minimum number of required boxes in the hit. In this case two.
    ///</summary>
    protected override int GetMinBoxesInHit()
    {
        return 2;
    }

    #endregion

    #region GetPriority

    public override int GetPriority()
    {
        return 0;
    }

    #endregion

    #region GetPoints

    ///<summary>
    ///Returns number of points assigned to the hit. In this case 20 points;
    ///</summary>
    public override int GetPoints()
    {
        return 20;
    }

    #endregion
}
