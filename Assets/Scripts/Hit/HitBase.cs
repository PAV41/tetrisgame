﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Abstract class which represents hit.
///</summary>
public abstract class HitBase : IHit
{
    #region InitialBox

    protected IBox _initialBox;

    #endregion

    #region BoxesInHit

    protected IList<IBox> _boxesInHit = new List<IBox>();

    #endregion

    #region CanExecute

    ///<summary>
    ///Checks if hit can be executed. Initializes internal list with boxes.
    ///</summary>
    public abstract bool CanExecute(IMatrix matrix, IBox box);

    #endregion

    #region Execute

    ///<summary>
    ///Executes hit.
    ///</summary>
    public abstract void Execute(IMatrix matrix);

    #endregion

    #region GetBoxesInHit

    ///<summary>
    ///Returs all boxes which are member of current hit.
    ///</summary>
    public IList<IBox> GetBoxesInHit()
    {
        return _boxesInHit;
    }

    #endregion

    #region GetMinBoxesInHit

    ///<summary>
    ///Returns minimum number of required boxes in the hit.
    ///</summary>
    protected abstract int GetMinBoxesInHit();

    #endregion

    #region GetHitShapeType

    ///<summary>
    ///Checks hit's shape. Currently only two shapes are recognized: horizontal or vertical.
    ///</summary>
    public HitShapeTypeEnum GetHitShapeType()
    {
        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        bool areXtheSame = _boxesInHit.Select(b => b.LogicalX).Distinct().Count() == 1;
        bool areYtheSame = _boxesInHit.Select(b => b.LogicalY).Distinct().Count() == 1;

        if (areXtheSame && !areYtheSame)
            return HitShapeTypeEnum.Vertical;
        else if (areYtheSame && !areXtheSame)
            return HitShapeTypeEnum.Horizontal;
        else
            return HitShapeTypeEnum.Unrecognized;
    }

    #endregion

    #region GetPriority

    public abstract int GetPriority();

    #endregion

    #region GetPoints

    ///<summary>
    ///Returns number of points assigned to the hit.
    ///</summary>
    public abstract int GetPoints();

    #endregion

    #region TryGetTheMostRelevantSpecialBox

    ///<summary>
    ///Returns the most relevant special box in the current hit.
    ///</summary>
    protected IBox TryGetTheMostRelevantSpecialBox()
    {
        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        IBox specialBox = null;

        specialBox = _boxesInHit.FirstOrDefault(b => b.HasProperty(BoxPropertyEnum.CanDestroyAnyBoxType));
        if (specialBox != null)
            return specialBox;

        specialBox = _boxesInHit.FirstOrDefault(b => b.HasProperty(BoxPropertyEnum.CanDestroy3x3Area));
        if (specialBox != null)
            return specialBox;

        specialBox = _boxesInHit.FirstOrDefault(b => b.HasProperty(BoxPropertyEnum.CanDestroyRowOrColumn));
        if (specialBox != null)
            return specialBox;

        return specialBox;
    }

    #endregion

    #region ApplySpecialBoxProperty

    ///<summary>
    ///Applies special box proeprty on the matrix.
    ///</summary>
    protected void ApplySpecialBoxProperty(IMatrix matrix, IBox specialBox)
    {
        if (matrix == null)
            throw new System.Exception("Matrix cannot be null");

        if (specialBox == null)
            throw new System.Exception("Special box cannot be null");

        if (specialBox.IsStandard)
            throw new System.Exception("Provied box does not have any property. This is not special box");

        if (!_boxesInHit.Any())
            throw new System.Exception("There is no box in current hit. Please initialize hit via CanExecute method!");

        var properties = specialBox.GetProperties();

        foreach (var property in properties)
        {
            if(property == BoxPropertyEnum.CanDestroyRowOrColumn)
            {
                var shapeType = GetHitShapeType();

                if (shapeType == HitShapeTypeEnum.Horizontal)
                {
                    var boxesInRow = matrix.GetAllBoxesInTheSameRow(specialBox);
                    if (boxesInRow.Any())
                    {
                        foreach (var boxInRow in boxesInRow)
                            matrix.UnregisterBox(boxInRow);
                    }
                }
                else if (shapeType == HitShapeTypeEnum.Vertical)
                {
                    var boxesInColumn = matrix.GetAllBoxesInTheSameColumn(specialBox);
                    if (boxesInColumn.Any())
                    {
                        foreach (var boxInCol in boxesInColumn)
                            matrix.UnregisterBox(boxInCol);
                    }
                }
                else
                    throw new System.Exception(string.Format("Cannot execute hit: {0}. Unrecognized hit shape type.", this.ToString()));

            }

            if (property == BoxPropertyEnum.CanDestroyAnyBoxType)
            {
                var allBoxes = matrix.GetAllBoxes();
                if (allBoxes.Any())
                {
                    var firstRegularBox = _boxesInHit.FirstOrDefault(b => b.IsStandard);
                    if (firstRegularBox == null)
                        throw new System.Exception("There is no regular box");

                    var boxesToRemove = new List<IBox>();
                    foreach (var box in allBoxes)
                    {
                        if (box.GetType() == firstRegularBox.GetType())
                            boxesToRemove.Add(box);
                    }
                    if (boxesToRemove.Any())
                    {
                        for (int i = 0; i < boxesToRemove.Count(); i++)
                            matrix.UnregisterBox(boxesToRemove.ElementAt(i));
                    }
                }
            }

            if(property == BoxPropertyEnum.CanDestroy3x3Area)
            {
                int maxY = specialBox.LogicalY + 2;
                if (maxY > matrix.MaximumLogicalY)
                    throw new System.Exception("Cannot execute property CanDestroy3x3Area. L shape height is beyond matrix's height.");

                int maxX = specialBox.LogicalX + 2;
                if(maxX > matrix.MaximumLogicalX)
                    throw new System.Exception("Cannot execute property CanDestroy3x3Area. L shape width is beyond matrix's width.");

                for (int x = maxX; x >= specialBox.LogicalX; x--)
                {
                    for(int y = maxY; y >= specialBox.LogicalY; y--)
                    {
                        var box = matrix.GetBox(x, y);
                        if (box != null)
                            matrix.UnregisterBox(box);
                    }
                }
            }
        }
    }

    #endregion

    #region ChooiceInitialBox

    ///<summary>
    ///Returns box which has been really moved by the user.
    ///</summary>
    protected IBox ChooiceInitialBox(IList<IBox> boxesInHit, IBox defaultBox)
    {
        if (boxesInHit == null)
            throw new System.Exception("Boxes in hit list cannot be null");

        if (defaultBox == null)
            throw new System.Exception("DefaultBox cannot be null");

        if (!boxesInHit.Any())
            throw new System.Exception("There is no boxes in the hit");

        var lastSelectedBox = boxesInHit.Where(b => b.LastSelectionTime != null).OrderByDescending(b => b.LastSelectionTime).FirstOrDefault(); //get last selected box if exists
        if (lastSelectedBox != null)
            return lastSelectedBox;
        else
            return defaultBox;
    }

    #endregion

    #region ToString

    public override string ToString()
    {
        string msg = string.Empty;
        if(!_boxesInHit.Any())
        {
            msg = string.Format("Hit: {0} which has no boxes", this.GetType());
        }
        else
        {
            msg += string.Format("Hit: {0}\n. Contains the following boxes:\n", this.GetType());
            int i = 1;
            foreach (var box in _boxesInHit)
            {
                msg += string.Format("{0}) {1}\n", i.ToString(), box.ToString());
                i++;
            }
        }
        return msg;
    }

    #endregion
}
