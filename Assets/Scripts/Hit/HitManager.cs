﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

///<summary>
///Static class which manages hits which are relevant after boxes movement.
///</summary>
public static class HitManager
{
    #region HitsTypesList

    private static IList<HitTypeEnum> _hitTypes = new List<HitTypeEnum>();

    #endregion

    #region HitsCount

    ///<summary>
    ///Returns number of registered hits types.
    ///</summary>
    public static int HitTypesCount
    {
        get
        {
            if (_hitTypes == null)
                return 0;

            if (!_hitTypes.Any())
                return 0;

            return _hitTypes.Count;
        }
    }

    #endregion

    #region ClearHitTypes

    ///<summary>
    ///Removes all registered hits types.
    ///</summary>
    public static void ClearHitTypes()
    {
        if (_hitTypes.Any())
        {
            _hitTypes = new List<HitTypeEnum>();
            Debug.Log("All hits has been removed");
        }
    }

    #endregion

    #region GetHitsInstances

    ///<summary>
    ///Returns all instances of registered hits types in correct ordering.
    ///</summary>
    public static IList<IHit> GetHitsInstances()
    {
        if (!_hitTypes.Any())
            throw new Exception("At least one hit type has to be registered first");

        var hitsList = new List<IHit>();
        foreach (var hitType in _hitTypes)
        {
            var hit = HitFactory.CreateHit(hitType);
            hitsList.Add(hit);
        }

        return hitsList.OrderByDescending(h => h.GetPriority()).ToList(); //The most complicated hits first
    }

    #endregion

    #region RegisterHitType

    ///<summary>
    ///Registers new hit type.
    ///</summary>
    public static void RegisterHitType(HitTypeEnum hit)
    {
        bool alreadyExists = _hitTypes.Contains(hit);
        if (alreadyExists)
            throw new Exception(string.Format("Hit: {0} has been already registered", hit));

        _hitTypes.Add(hit);

        Debug.Log(string.Format("{0} hit has been added", hit));
    }

    #endregion

    #region ExecuteHits

    ///<summary>
    ///Executes all hits which are existing in current state of matrix. Repeats operation until there is no more hit.
    ///</summary>
    public static void ExecuteHits(IMatrix matrix)
    {
        if (matrix == null)
            throw new Exception("Matrix cannot be null");

        IHit hit = null;
        while ((hit = FindNextHit(matrix)) != null)
        {
            hit.Execute(matrix);
#if DEBUG   //!!!!!!!!!!do it only in debug mode !!!!!!!!!!
            DebbugerHelper.CaptureScreenShoot();
#endif
        }
    }

    #endregion

    #region FindHits

    ///<summary>
    ///Finds next possible hit in current state of matrix.
    ///</summary>
    private static IHit FindNextHit(IMatrix matrix)
    {
        if (matrix == null)
            throw new Exception("Matrix cannot be null");

        IHit result = null;

        var hits = new List<IHit>();

        var boxesInMatrix = matrix.GetAllBoxes().Where(b => b.GetType() != typeof(CanDestroyAnyBoxTypeBox));
        if(boxesInMatrix.Any())
        {
            foreach (var box in boxesInMatrix)
            {
                var hitsInstances = GetHitsInstances();
                if(hitsInstances.Any())
                {
                    foreach (var hitInstance in hitsInstances)
                    {
                        if (hitInstance.CanExecute(matrix, box))
                        {
                            var theSameHits = hits.Where(h => h.GetBoxesInHit() != null && h.GetBoxesInHit().Contains(box)); // only unique elements
                            if(!theSameHits.Any())
                                hits.Add(hitInstance);
                            else
                            {
                                var maxPriorityTheSameHits = theSameHits.Max(h => h.GetPriority());
                                if (hitInstance.GetPriority() > maxPriorityTheSameHits)
                                {
                                    hits.RemoveAll(h => theSameHits.Contains(h));
                                    hits.Add(hitInstance);
                                 }
                            }
                        }
                    }
                }
            }
        }

        if (hits.Any())
            result = hits.OrderByDescending(h => h.GetPriority()).First();

        return result;
    }

#endregion
}
