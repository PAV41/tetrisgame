﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrayBox : BoxBase
{
    public GrayBox()
    {

    }

    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.GrayPrefab);
    }

    public override bool IsActive()
    {
        return true;
    }
}
