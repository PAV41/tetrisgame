﻿using System;
using UnityEngine;

///<summary>
///Represents blue box.
///</summary>
public class BlueBox : BoxBase
{
    public BlueBox()
    {

    }

	///<summary>
	///Blue box is active.
	///</summary>
    public override bool IsActive()
    {
        return true;
    }

	///<summary>
	///Returns path to blue box prefab object in Unity project.
	///</summary>
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.BluePrefab);
    }
}
