﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrayCanDestroy3x3AreaBox : BoxBase
{
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.GrayCanDestroy3x3AreaPrefab);
    }

    public override bool IsActive()
    {
        return true;
    }

    public override IList<BoxPropertyEnum> GetProperties()
    {
        return new List<BoxPropertyEnum>
        {
            BoxPropertyEnum.CanDestroy3x3Area
        };
    }
}
