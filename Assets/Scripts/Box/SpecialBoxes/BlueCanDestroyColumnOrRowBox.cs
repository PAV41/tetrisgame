﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueCanDestroyColumnOrRowBox : BoxBase
{
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.BlueCanDestroyColumnOrRowPrefab);
    }

    public override bool IsActive()
    {
        return true;
    }

    public override IList<BoxPropertyEnum> GetProperties()
    {
        return new List<BoxPropertyEnum>
        {
            BoxPropertyEnum.CanDestroyRowOrColumn
        };
    }
}
