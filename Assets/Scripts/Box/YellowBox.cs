﻿using System;
using UnityEngine;

///<summary>
///Represents yellow box.
///</summary>
public class YellowBox : BoxBase
{
    public YellowBox()
    {

    }

	///<summary>
	///Yellow box is active.
	///</summary>
    public override bool IsActive()
    {
        return true;
    }

	///<summary>
	///Returns path to yellow box prefab object in Unity project.
	///</summary>
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.YellowPrefab);
    }
}