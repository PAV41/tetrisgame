﻿using System;
using UnityEngine;

///<summary>
///Represents red box.
///</summary>
public class RedBox : BoxBase
{
    public RedBox()
    {
    }

	///<summary>
	///Red box is active.
	///</summary>
    public override bool IsActive()
    {
        return true;
    }

	///<summary>
	///Returns path to red box prefab object in Unity project.
	///</summary>
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.RedPrefab);
    }
}
