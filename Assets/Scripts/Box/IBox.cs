﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IBox {

    int LogicalX { get; set; }

    int LogicalY { get; set; }

    int LogicalWidth { get; set; }

    int LogicalHeight { get; set; }

    DateTime? LastSelectionTime { get; }

    bool IsStandard { get; }

    ///<summary>
    ///Initializes box's parameters: position X, Y, width, height.
    ///</summary>
    void Initialize(int logicalX, int logicalY);

    ///<summary>
    ///Returns box's properties.
    ///</summary>
    IList<BoxPropertyEnum> GetProperties();

    ///<summary>
    ///Checks if box has property.
    ///</summary>
    bool HasProperty(BoxPropertyEnum proeprty);

    ///<summary>
    ///Creates new physical game object from assigned prefab.
    ///</summary>
    void CreatePhysicalBox();
   
    ///<summary>
    ///Destroys assigned physical game object.
    ///</summary>
    void DestroyPhysicalBox();


    ///<summary>
    ///Updates assigned physical game object position.
    ///</summary>
    void UpdatePhysicalBoxPosition();

    ///<summary>
    ///Marks box as again selected.
    ///</summary>
    void SelectBox();

    ///<summary>
    ///Deselects box.
    ///</summary>
    void DeSelectBox();

    ///<summary>
    ///Returns how many times box has been selected.
    ///</summary>
    int GetSelectedTimesCount();

    ///<summary>
    ///Determines if box is only background or not.
    ///</summary>
    bool IsActive();

    ///<summary>
    ///Moves physical game object- function's parameters are movement description.
    ///</summary>
    void MovePhysicalBox(int moveLogicalX, int moveLogicalY);

    ///<summary>
    ///Moves physical game object one cell down.
    ///</summary>
    void MovePhysicalBoxDown();

    ///<summary>
    ///Moves physical game object one cell up.
    ///</summary>
    void MovePhysicalBoxUp();

    ///<summary>
    ///Moves physical game object one cell left.
    ///</summary>
    void MovePhysicalBoxLeft();

    ///<summary>
    ///Moves physical game object one cell right.
    ///</summary>
    void MovePhysicalBoxRight();
}
