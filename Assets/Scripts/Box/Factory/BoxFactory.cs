﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Helper static class (factory) responsible for boxes instances creation.
///</summary>
public static class BoxFactory {

    ///<summary>
	///Creates various boxes instances.
	///</summary>
	public static IBox CreateBox(BoxTypeEnum type, int logicalX, int logicalY)
    {
        IBox box = null;
        switch (type)
        {
            case BoxTypeEnum.BlueBox:
                box = ScriptableObject.CreateInstance<BlueBox>();
                break;
            case BoxTypeEnum.RedBox:
                box = ScriptableObject.CreateInstance<RedBox>();
                break;
            case BoxTypeEnum.YellowBox:
                box = ScriptableObject.CreateInstance<YellowBox>();
                break;
            case BoxTypeEnum.GreenBox:
                box = ScriptableObject.CreateInstance<GreenBox>();
                break;
            case BoxTypeEnum.VioletBox:
                box = ScriptableObject.CreateInstance<VioletBox>();
                break;
            case BoxTypeEnum.GrayBox:
                box = ScriptableObject.CreateInstance<GrayBox>();
                break;
            case BoxTypeEnum.BlueCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<BlueCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.RedCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<RedCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.YellowCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<YellowCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.GreenCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<GreenCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.VioletCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<VioletCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.GrayCanDestroyColumnOrRowBox:
                box = ScriptableObject.CreateInstance<GrayCanDestroyColumnOrRowBox>();
                break;
            case BoxTypeEnum.CanDestroyAnyBoxTypeBox:
                box = ScriptableObject.CreateInstance<CanDestroyAnyBoxTypeBox>();
                break;
            case BoxTypeEnum.BlueCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<BlueCanDestroy3x3AreaBox>();
                break;
            case BoxTypeEnum.GrayCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<GrayCanDestroy3x3AreaBox>();
                break;
            case BoxTypeEnum.GreenCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<GreenCanDestroy3x3AreaBox>();
                break;
            case BoxTypeEnum.RedCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<RedCanDestroy3x3AreaBox>();
                break;
            case BoxTypeEnum.VioletCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<VioletCanDestroy3x3AreaBox>();
                break;
            case BoxTypeEnum.YellowCanDestroy3x3AreaBox:
                box = ScriptableObject.CreateInstance<YellowCanDestroy3x3AreaBox>();
                break;
            default:
                break;
        }

        if (box != null)
            box.Initialize(logicalX, logicalY);

        return box;
    }
}
