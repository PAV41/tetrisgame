﻿using System;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Represents abstract model of single box
///</summary>
public abstract class BoxBase : ScriptableObject, IBox
{
    #region GameObject

    public UnityEngine.Object GameObject;

    #endregion

    #region LastSelectionTime

    private DateTime? _lastSelectionTime;

    public DateTime? LastSelectionTime
    {
        get
        {
            return _lastSelectionTime;
        }
    }

    #endregion

    #region IsStandard

    public bool IsStandard
    {
        get
        {
            return this.GetProperties().Count == 0 ? true : false;
        }
    }

    #endregion

    #region AssignedGameObject

    public GameObject AssignedGetGameObject
    {
        get
        {
            return GameObject != null ? (GameObject as GameObject) : null;
        }
    }

    #endregion

    #region SelectedTimesCounter

    private int _selectedTimesCounter;

    #endregion

    #region LogicalX

    private int _logicalX;

    public int LogicalX
    {
        get
        {
            return _logicalX;
        }
        set
        {
            _logicalX = value;
        }
    }

    #endregion

    #region LogicalY

    private int _logicalY;

    public int LogicalY
    {
        get
        {
            return _logicalY;
        }
        set
        {
            _logicalY = value;
        }
    }

    #endregion

    #region LogicalHeight

    private int _logicalHeight;

    public int LogicalHeight
    {
        get
        {
            return _logicalHeight;
        }
        set
        {
            if (value <= 0)
                throw new Exception("Logical height has to be greater than 0");

            _logicalHeight = value;
        }
    }

    #endregion

    #region LogicalWidth

    private int _logicalWidth;

    public int LogicalWidth
    {
        get
        {
            return _logicalWidth;
        }
        set
        {
            if (value <= 0)
                throw new Exception("Logical width has to be greater than 0");

            _logicalWidth = value;
        }
    }

    #endregion

    #region Initialize

    public void Initialize(int logicalX, int logicalY)
    {
        this.LogicalX = logicalX;
        this.LogicalY = logicalY;
        this.LogicalHeight = 1;
        this.LogicalWidth = 1;
    }

    #endregion

    #region GetBoxProperties

    public virtual IList<BoxPropertyEnum> GetProperties()
    {
        return new List<BoxPropertyEnum>();
    }

    #endregion

    #region HasProperty

    public bool HasProperty(BoxPropertyEnum property)
    {
        if (IsStandard)
            return false;

        return GetProperties().Contains(property);
    }

    #endregion

    #region CreatePhysicalBox

    public void CreatePhysicalBox()
    {
        string prefabName = GetRegularPrefabName();
        if (string.IsNullOrEmpty(prefabName))
            throw new Exception(string.Format("Cannot create physical box: {0}. Prefab name is empty.", this.GetType().Name));

        var prefab = Resources.Load(prefabName) as GameObject;
        if (prefab == null)
            throw new Exception(string.Format("Cannot create physical box: {0}. Prefab object not found.", this.GetType().Name));
        else
        {
            GameObject = Instantiate(prefab, new Vector3(LogicalX, LogicalY, 0), Quaternion.identity);
        }
    }

    #endregion

    #region DestroyPhysicalBox

    public virtual void DestroyPhysicalBox()
    {
        if (GameObject != null)
        {
            Destroy(GameObject);
        }
    }

    #endregion

    #region UpdatePhysicalBoxPosition

    public void UpdatePhysicalBoxPosition()
    {
        if (GameObject == null)
            throw new Exception("Game object has not been assigned");

        (GameObject as GameObject).transform.position = new Vector3(LogicalX, LogicalY, 0);
    }

    #endregion

    #region IsActive

    public abstract bool IsActive();

    #endregion

    #region RegularPrefabName

    public abstract string GetRegularPrefabName();

    #endregion

    #region MovePhysicalBox

    public void MovePhysicalBox(int moveLogicalX, int moveLogicalY)
    {
        if (moveLogicalX == 0 && moveLogicalY == 0)
            throw new Exception(string.Format("Cannot move Box at position: X = {0} and Y = {1}. Movement X or Y has to be greater than 0", LogicalX, LogicalY));

        if (GameObject == null)
            throw new Exception("Physical object has not been assigned.");

        if(moveLogicalX != 0)
        {
            LogicalX += moveLogicalX;
            (GameObject as GameObject).transform.Translate(moveLogicalX, 0, 0);
        }

        if(moveLogicalY != 0)
        {
            LogicalY += moveLogicalY;
            (GameObject as GameObject).transform.Translate(0, moveLogicalY, 0);
        }
    }

    #endregion

    #region SelectBox

    public void SelectBox()
    {
        _selectedTimesCounter++;
        _lastSelectionTime = DateTime.Now;
    }

    #endregion

    #region DeSelectBox

    public void DeSelectBox()
    {
        _selectedTimesCounter = 0;
        _lastSelectionTime = null;
    }

    #endregion

    #region GetSelectedTimesCount

    public int GetSelectedTimesCount()
    {
        return _selectedTimesCounter;
    }

    #endregion

    #region ToString

    public override string ToString()
    {
        return string.Format("Box: InstanceId: {0} X = {1} Y = {2} Type: {3} Selected Times: {4} Selection Time: {5}", 
                             GameObject == null ? "Undefined" : GameObject.GetInstanceID().ToString(), 
                             LogicalX, LogicalY, GetType().Name, GetSelectedTimesCount(), 
                             LastSelectionTime.HasValue ? LastSelectionTime.Value.ToLongTimeString() : string.Empty);
    }

    #endregion

    #region Equals

    public override bool Equals(object other)
    {
        return this.LogicalX == (other as IBox).LogicalX && this.LogicalY == (other as IBox).LogicalY;
    }

    #endregion

    #region MovePhysicalBoxDown

    public void MovePhysicalBoxDown()
    {
        MovePhysicalBox(0, -1);
    }

    #endregion

    #region MovePhysicalBoxUp

    public void MovePhysicalBoxUp()
    {
        MovePhysicalBox(0, 1);
    }

    #endregion

    #region MovePhysicalBoxLeft

    public void MovePhysicalBoxLeft()
    {
        MovePhysicalBox(-1, 0);
    }

    #endregion

    #region MovePhysicalBoxRight

    public void MovePhysicalBoxRight()
    {
        MovePhysicalBox(1, 0);
    }

    #endregion
}
