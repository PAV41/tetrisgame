﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VioletBox : BoxBase
{
    public VioletBox()
    {

    }

    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.VioletPrefab);
    }

    public override bool IsActive()
    {
        return true;
    }
}
