﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenBox : BoxBase
{
    public GreenBox()
    {
    }

    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.GreenPrefab);
    }

    public override bool IsActive()
    {
        return true;
    }
}
