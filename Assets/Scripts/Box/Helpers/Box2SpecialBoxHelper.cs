﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Box2SpecialBoxHelper
{
    #region MappingList

    private static IList<Box2SpecialBoxMapping> _boxMappings = new List<Box2SpecialBoxMapping>
    {
        new Box2SpecialBoxMapping(typeof(BlueBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(BlueBox), typeof(BlueCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(BlueBox), typeof(BlueCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(BlueCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(RedBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(RedBox), typeof(RedCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(RedBox), typeof(RedCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(RedCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(YellowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(YellowBox), typeof(YellowCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(YellowBox), typeof(YellowCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(YellowCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(GreenBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(GreenBox), typeof(GreenCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(GreenBox), typeof(GreenCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(GreenCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(GrayBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(GrayBox), typeof(GrayCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(GrayBox), typeof(GrayCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(GrayCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(VioletBox), typeof(CanDestroyAnyBoxTypeBox)),
        new Box2SpecialBoxMapping(typeof(VioletBox), typeof(VioletCanDestroyColumnOrRowBox)),
        new Box2SpecialBoxMapping(typeof(VioletBox), typeof(VioletCanDestroy3x3AreaBox)),
        new Box2SpecialBoxMapping(typeof(VioletCanDestroyColumnOrRowBox), typeof(CanDestroyAnyBoxTypeBox)),
    };

    #endregion

    #region FindMapping

    public static Box2SpecialBoxMapping FindMapping(Type box1Type, Type box2Type)
    {
        if (box1Type == null)
            throw new Exception("Box1 type cannot be null");

        if (box2Type == null)
            throw new Exception("Box2 type cannot be null");

        if (!_boxMappings.Any())
            throw new Exception("There is no maping defined");

        var mapping = _boxMappings.FirstOrDefault(m => (m.BaseBoxType == box1Type && m.SpecialBoxType == box2Type) || (m.BaseBoxType == box2Type && m.SpecialBoxType == box1Type));

        return mapping;
    }

    #endregion

    #region AreBoxesTheSameType

    public static bool AreBoxesTheSameType(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        if (!_boxMappings.Any())
            throw new Exception("There is no maping defined");

        var mapping = FindMapping(box1.GetType(), box2.GetType());

        return mapping != null ? true : false;
    }

    #endregion
}
