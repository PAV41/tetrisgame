﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box2SpecialBoxMapping 
{
    #region BaseBoxType

    public Type BaseBoxType { get; set; }

    #endregion

    #region SpecialBoxType

    public Type SpecialBoxType { get; set; }

    #endregion

    #region Ctor

    public Box2SpecialBoxMapping(Type baseBoxType, Type specialBoxType)
    {
        BaseBoxType = baseBoxType;
        SpecialBoxType = specialBoxType;
    }

    #endregion
}
