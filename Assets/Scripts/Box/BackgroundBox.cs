﻿
///<summary>
///Represents background box.
///</summary>
public class BackgroundBox : BoxBase
{
    public BackgroundBox(int logicalX, int logicalY)
    {

    }

	///<summary>
	///Background box is not active.
	///</summary>
    public override bool IsActive()
    {
        return false;
    }

	///<summary>
	///Returns path to background box prefab object in Unity project.
	///</summary>
    public override string GetRegularPrefabName()
    {
        return string.Format("{0}/{1}", Const.Directories.Prefabs, Const.Prefabs.BackgroundPrefab);
    }
}