﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameLogic : MonoBehaviour
{

    public static IMatrix CurrentMatrix;

    // Use this for initialization
    void Start()
    {

        int height = CameraHelper.GetCameraHeight();
        int width = CameraHelper.GetCameraWidth();

        IMatrix matrix = MatrixFactory.CreateMatrix(MatrixTypeEnum.MatrixLevelOne, width, height);
        matrix.GenerateMatrix();

        CurrentMatrix = matrix;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            var logicalPosition = MouseHelper.GetMouseLogicalPosition();
            if (logicalPosition != null)
            {
                var selectedBox = CurrentMatrix.GetBox((int)logicalPosition.x, (int)logicalPosition.y);
                if (selectedBox != null)
                {
                    selectedBox.SelectBox();

                    var selectedItems = CurrentMatrix.GetSelectedBoxes();
                    if (selectedItems != null && selectedItems.Count == 2)
                    {
                        IBox box1 = selectedItems.ElementAt(0);
                        IBox box2 = selectedItems.ElementAt(1);
                        bool areNeighbours = CurrentMatrix.AreBoxesNeighbours(box1, box2);
                        if (areNeighbours)
                        {
                            CurrentMatrix.ReplaceBoxes(box1, box2);
                            HitManager.ExecuteHits(CurrentMatrix);
                            box1.DeSelectBox();
                            box2.DeSelectBox();
                        }
                        else
                        {
                            box1.DeSelectBox();
                            box2.DeSelectBox();
                        }
                    }
                }
            }
        }
        else if (Input.GetMouseButtonDown(1))
        {
            var logicalPosition = MouseHelper.GetMouseLogicalPosition();
            var selectedBox = CurrentMatrix.GetBox((int)logicalPosition.x, (int)logicalPosition.y);
            if (selectedBox != null)
                CurrentMatrix.UnregisterBox(selectedBox);
        }
    }
}
