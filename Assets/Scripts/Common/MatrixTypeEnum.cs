﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MatrixTypeEnum  {

    MatrixLevelOne      =   1,
    MatrixLevelTwo      =   2,
    MatrixLevelThree    =   3,
    MatrixLevelFour     =   4,
    MatrixLevelFive     =   5,
    MatrixLevelSix      =   6,
    MatrixLevelSeven    =   7,
    MatrixLevelEight    =   8,
    MatrixLevelNine     =   9,
    MatrixLevelTen      =   10,
}
