﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoxTypeEnum {

	BlueBox                         = 0,
    RedBox                          = 1,
    YellowBox                       = 2,
    GreenBox                        = 3,
    VioletBox                       = 4,
    GrayBox                         = 5,
    BlueCanDestroyColumnOrRowBox    = 6,
    GrayCanDestroyColumnOrRowBox    = 7,
    GreenCanDestroyColumnOrRowBox   = 8,
    RedCanDestroyColumnOrRowBox     = 9,
    VioletCanDestroyColumnOrRowBox  = 10,
    YellowCanDestroyColumnOrRowBox  = 11,
    CanDestroyAnyBoxTypeBox         = 12,
    BlueCanDestroy3x3AreaBox        = 13,
    GrayCanDestroy3x3AreaBox        = 14,
    GreenCanDestroy3x3AreaBox       = 15,
    RedCanDestroy3x3AreaBox         = 16,
    VioletCanDestroy3x3AreaBox      = 17,
    YellowCanDestroy3x3AreaBox      = 18
}
