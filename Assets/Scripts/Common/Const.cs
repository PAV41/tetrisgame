﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Const {

    public static class Directories
    {
        public static readonly string Materials = "Materials";
        public static readonly string Prefabs = "Prefabs";
        public static readonly string Scripts = "Scripts";
        public static readonly string Scenes = "Scenes";
    }

    public static class PhysicalDirectories
    {
        public static readonly string ScreenShootsDirectory = @"C:\Temp\Tetris\Screens";
    }


    public static class Prefabs
    {
        public static readonly string BackgroundPrefab = "BackgroundBox";
        public static readonly string BluePrefab = "BluePrefab";
        public static readonly string RedPrefab = "RedPrefab";
        public static readonly string YellowPrefab = "YellowPrefab";
        public static readonly string GreenPrefab = "GreenPrefab";
        public static readonly string VioletPrefab = "VioletPrefab";
        public static readonly string GrayPrefab = "GrayPrefab";

        public static readonly string BlueCanDestroyColumnOrRowPrefab = "BlueCanDestroyColumnOrRowPrefab";
        public static readonly string RedCanDestroyColumnOrRowPrefab = "RedCanDestroyColumnOrRowPrefab";
        public static readonly string YellowCanDestroyColumnOrRowPrefab = "YellowCanDestroyColumnOrRowPrefab";
        public static readonly string GreenCanDestroyColumnOrRowPrefab = "GreenCanDestroyColumnOrRowPrefab";
        public static readonly string VioletCanDestroyColumnOrRowPrefab = "VioletCanDestroyColumnOrRowPrefab";
        public static readonly string GrayCanDestroyColumnOrRowPrefab = "GrayCanDestroyColumnOrRowPrefab";

        public static readonly string BlueCanDestroy3x3AreaPrefab = "BlueCanDestroy3x3AreaPrefab";
        public static readonly string RedCanDestroy3x3AreaPrefab = "RedCanDestroy3x3AreaPrefab";
        public static readonly string YellowCanDestroy3x3AreaPrefab = "YellowCanDestroy3x3AreaPrefab";
        public static readonly string GreenCanDestroy3x3AreaPrefab = "GreenCanDestroy3x3AreaPrefab";
        public static readonly string VioletCanDestroy3x3AreaPrefab = "VioletCanDestroy3x3AreaPrefab";
        public static readonly string GrayCanDestroy3x3AreaPrefab = "GrayCanDestroy3x3AreaPrefab";

        public static readonly string CanDestroyAnyBoxTypePrefab = "CanDestroyAnyBoxTypePrefab";
    }

}
