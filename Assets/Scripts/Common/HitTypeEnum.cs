﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HitTypeEnum
{
    TwoElementsHit   =  2,
    ThreeElementsHit =  3,
    FourElementsHit  =  4,
    FiveElementsHit  =  5,
    LHit             =  6,
}
