﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoxPropertyEnum
{
    CanDestroyRowOrColumn =  0,
    CanDestroyAnyBoxType  =  1,
    CanDestroy3x3Area     =  2
}
