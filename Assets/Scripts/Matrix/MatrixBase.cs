﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

///<summary>
///Represents abstract model of matrix
///</summary>
public abstract class MatrixBase : IMatrix
{
    private static IList<IBox> _boxes;

    #region ctor

    public MatrixBase(int logicalWidth, int logicalHeight)
    {
        _boxes = new List<IBox>();
        this.LogicalHeight = logicalHeight;
        this.LogicalWidth = logicalWidth;
        RegisterMatrixConditions();
        RegisterAvailableHits();
    }

    #endregion

    #region GenerateMatrix

    public abstract void GenerateMatrix();

    #endregion

    #region LogicalHeight

    private int _logicalHeight;

    public int LogicalHeight
    {
        get
        {
            return _logicalHeight;
        }
        set
        {
            _logicalHeight = value;
        }
    }

    #endregion

    #region LogicalWidth

    private int _logicalWidth;

    public int LogicalWidth
    {
        get
        {
            return _logicalWidth;
        }
        set
        {
            _logicalWidth = value;
        }
    }

    #endregion

    #region MinimumLogicalX

    public int MinimumLogicalX
    {
        get
        {
            return (-1) * (int)(LogicalWidth / 2);
        }
    }

    #endregion

    #region MaximumLogicalX

    public int MaximumLogicalX
    {
        get
        {
            return (int)(LogicalWidth / 2);
        }
    }

    #endregion

    #region MinimumLogicalY

    public int MinimumLogicalY
    {
        get
        {
            return (-1) * (int)(LogicalHeight / 2);
        }
    }

    #endregion

    #region MaximumLogicalY

    public int MaximumLogicalY
    {
        get
        {
            return (int)(LogicalHeight / 2);
        }
    }

    #endregion

    #region RegisterMatrixConditions

    public abstract void RegisterMatrixConditions();

    #endregion

    #region RegisterMatrixConditions

    public abstract void RegisterAvailableHits();

    #endregion

    #region GetBox

    public IBox GetBox(int logicalX, int logicalY)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        return _boxes.SingleOrDefault(b => b.LogicalX == logicalX && b.LogicalY == logicalY);
    }

    #endregion

    #region GetAllBoxes

    public IList<IBox> GetAllBoxes()
    {
        return _boxes;
    }

    #endregion

    #region GetSelectedBoxes

    public IList<IBox> GetSelectedBoxes()
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        return _boxes.Where(b => b.GetSelectedTimesCount() > 0 && b.LastSelectionTime != null).OrderBy(b => b.LastSelectionTime).ToList();
    }

    #endregion

    #region RegisterBox

    public void RegisterBox(IBox box)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box == null)
            throw new Exception("Box cannot be null");

        if (box.LogicalX > LogicalWidth)
            throw new Exception(string.Format("Cannot register box. Maximum width is: {0}, but box width is: {1}", LogicalWidth, box.LogicalX));

        if (box.LogicalY > LogicalHeight)
            throw new Exception(string.Format("Cannot register box. Maximum height is: {0}, but box height is: {1}", LogicalHeight, box.LogicalY));

        var existingBox = _boxes.SingleOrDefault(b => b.LogicalX == box.LogicalX && b.LogicalY == box.LogicalY);
        if (existingBox != null)
            throw new Exception(string.Format("Matrix already has box at position X = {0} Y = {1}", box.LogicalX, box.LogicalY));

        box.CreatePhysicalBox();
        _boxes.Add(box);

    }

    #endregion

    #region UnregisterBox

    public void UnregisterBox(IBox box)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box == null)
            throw new Exception("Box cannot be null");

        var existingBox = _boxes.SingleOrDefault(b => b.LogicalX == box.LogicalX && b.LogicalY == box.LogicalY);
        if (existingBox == null)
            throw new Exception(string.Format("Matrix doesn't have box at position X = {0} Y = {1}", box.LogicalX, box.LogicalY));

        box.DestroyPhysicalBox();
        _boxes.Remove(box);
    }

    #endregion

    #region ClearMatrix

    public void ClearMatrix()
    {
        if (_boxes == null || _boxes.Count == 0)
            return;

        foreach (var box in _boxes)
            box.DestroyPhysicalBox();

        _boxes = new List<IBox>();
    }

    #endregion

    #region MoveBox

    public void MoveBox(int logicalX, int logicalY, int moveLogicalX, int moveLogicalY)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        var existingBox = _boxes.SingleOrDefault(b => b.LogicalX == logicalX && b.LogicalY == logicalY);
        if (existingBox == null)
            throw new Exception(string.Format("Matrix doesn't have box at position X = {0} Y = {1}", logicalX, logicalY));

        //existingBox.SelectBox();
        existingBox.MovePhysicalBox(moveLogicalX, moveLogicalY);
    }

    #endregion

    #region ReplaceBoxes

    public void ReplaceBoxes(IBox box1, IBox box2)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        int box1X = box1.LogicalX;
        int box1Y = box1.LogicalY;

        int box2X = box2.LogicalX;
        int box2Y = box2.LogicalY;

        box1.LogicalX = box2X;
        box1.LogicalY = box2Y;

        box2.LogicalX = box1X;
        box2.LogicalY = box1Y;

        box1.UpdatePhysicalBoxPosition();
        box2.UpdatePhysicalBoxPosition();
    }

    #endregion

    #region GetBoxesNeighbours

    public IList<IBox> GetBoxesNeighbours(IBox box)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box == null)
            throw new Exception("Box cannot be null");

        return _boxes.Where(b => AreBoxesNeighbours(b, box) && b.IsActive()).ToList();
    }

    #endregion

    #region AreBoxesNeighbours

    public bool AreBoxesNeighbours(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        if (box1.LogicalX == box2.LogicalX && box1.LogicalY == box2.LogicalY)
            return false;

        var distance = Math.Sqrt(Math.Pow((box1.LogicalX - box2.LogicalX), 2) + Math.Pow((box1.LogicalY - box2.LogicalY), 2));

        return distance <= 1;
    }

    #endregion

    #region AreBoxesTheSameType

    public bool AreBoxesTheSameType(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        bool areTheSame = box1.GetType() == box2.GetType();
        if (areTheSame)
            return true;
        
        return Box2SpecialBoxHelper.AreBoxesTheSameType(box1, box2);
    }

    #endregion

    #region IsEmptyCell

    public bool IsEmptyCell(int logicalPosX, int logicalPosY)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        var box = GetBox(logicalPosX, logicalPosY);

        return box == null;
    }

    #endregion

    #region IsUpCellEmpty

    public bool IsUpCellEmpty(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        return IsEmptyCell(box.LogicalX, box.LogicalY + 1);
    }

    #endregion

    #region IsBelowCellEmpty

    public bool IsBelowCellEmpty(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        return IsEmptyCell(box.LogicalX, box.LogicalY - 1);
    }

    #endregion

    #region IsLeftCellEmpty

    public bool IsLeftCellEmpty(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        return IsEmptyCell(box.LogicalX - 1, box.LogicalY);
    }

    #endregion

    #region IsRightCellEmpty

    public bool IsRightCellEmpty(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        return IsEmptyCell(box.LogicalX + 1, box.LogicalY);
    }

    #endregion

    #region AreBoxesInTheSameColumn

    public bool AreBoxesInTheSameColumn(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalX == box2.LogicalX;
    }

    #endregion

    #region AreBoxesInTheSameRow

    public bool AreBoxesInTheSameRow(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalY == box2.LogicalY;
    }

    #endregion

    #region IsBoxAbove

    public bool IsBoxAbove(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalY > box2.LogicalY;
    }

    #endregion

    #region IsBoxBelow

    public bool IsBoxBelow(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalY < box2.LogicalY;
    }

    #endregion

    #region IsBoxOnTheRight

    ///<summary>
    ///Checks if first box on the right side of second box.
    ///</summary>
    public bool IsBoxOnTheRight(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalX > box2.LogicalX;
    }

    #endregion

    #region IsBoxOnTheLeft

    public bool IsBoxOnTheLeft(IBox box1, IBox box2)
    {
        if (box1 == null)
            throw new Exception("Box1 cannot be null");

        if (box2 == null)
            throw new Exception("Box2 cannot be null");

        return box1.LogicalX < box2.LogicalX;
    }

    #endregion

    #region UpdateMatrixAfterRemovement

    public void UpdateMatrixAfterRemovement(IBox removedBox)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (removedBox == null)
            throw new Exception("Removed Box cannot be null");

        var existingBox = _boxes.SingleOrDefault(b => b.LogicalX == removedBox.LogicalX && b.LogicalY == removedBox.LogicalY);
        if (existingBox != null)
            throw new Exception(string.Format("Box at position: X = {0} Y = {1} has not been removed", removedBox.LogicalX, removedBox.LogicalY));

        IList<IBox> itemsToMove = new List<IBox>();

        foreach (var box in _boxes)
        {
            bool isTheSameCol = AreBoxesInTheSameColumn(box, removedBox);
            bool isboxAbove = IsBoxAbove(box, removedBox);
            if (isTheSameCol && isboxAbove)
                itemsToMove.Add(box);
        }

        if (itemsToMove.Any())
        {
            itemsToMove = itemsToMove.OrderBy(b => b.LogicalY).ToList();

            foreach (var boxToMove in itemsToMove)
            {
                bool isCellEmpty = IsBelowCellEmpty(boxToMove);
                if (isCellEmpty)
                    boxToMove.MovePhysicalBoxDown();
            }
        }
    }

    #endregion

    #region RemoveTheSameBoxesRecursively

    public void RemoveTheSameBoxesRecursively(IBox box) // gdzies w tej funkcji jest blad...
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        var neighbors = GetBoxesNeighbours(box);
        if (neighbors.Any())
        {
            foreach (var neighbor in neighbors)
            {
                bool areTheSame = AreBoxesTheSameType(box, neighbor);
                if (areTheSame)
                {
                    UnregisterBox(box);
                    UpdateMatrixAfterRemovement(box);
                    RemoveTheSameBoxesRecursively(neighbor); //Recursively call function again for the current neighbor
                }
            }
        }
        else
            UnregisterBox(box);
    }

    #endregion

    #region GetBoxesOfSameTypeInARow

    public IList<IBox> GetBoxesOfSameTypeInARow(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        IList<IBox> boxesInRow = new List<IBox>();

        for (int i = box.LogicalX - BoxesOfSameTypeToTheLeft(box); i <= box.LogicalX + BoxesOfSameTypeToTheRight(box); i++)
            boxesInRow.Add(GetBox(i,box.LogicalY));

        return boxesInRow;
    }

    #endregion

    #region GetBoxesOfSameTypeInAColumn

    public IList<IBox> GetBoxesOfSameTypeInAColumn(IBox box)
    {
        if (box == null)
            throw new Exception("Box cannot be null");

        IList<IBox> boxesInColumn = new List<IBox>();

        for (int i = box.LogicalY - BoxesOfSameTypeDown(box); i <= box.LogicalY + BoxesOfSameTypeUp(box); i++)
            boxesInColumn.Add(GetBox(box.LogicalX,i));

        return boxesInColumn;
    }

    #endregion

    #region BoxesOfSameTypeToTheLeft

    public int BoxesOfSameTypeToTheLeft(IBox box)
    {
        int sameCount = 0;
        if (box == null)
            throw new Exception("Box cannot be null");

        int i = box.LogicalX; 


        while (i > MinimumLogicalX)
        {
            if (IsEmptyCell(i - 1, box.LogicalY))     //tested box cannot be null or empty
                break;

            IBox boxToCheck = GetBox(i-1, box.LogicalY);

            //if (boxToCheck.GetType() == box.GetType())
            if (AreBoxesTheSameType(boxToCheck, box))
                sameCount++;
            else
                break;

            i--;
        }

        return sameCount;
    }

    #endregion

    #region BoxesOfSameTypeToTheRight

    public int BoxesOfSameTypeToTheRight(IBox box)
    {
        int sameCount = 0;
        if (box == null)
            throw new Exception("Box cannot be null");

        int i = box.LogicalX;

        while (i < MaximumLogicalX)
        {
            if (IsEmptyCell(i + 1, box.LogicalY))     //tested box cannot be null or empty
                break;

            IBox boxToCheck = GetBox(i+1, box.LogicalY);

            //if (boxToCheck.GetType() == box.GetType())
            if (AreBoxesTheSameType(boxToCheck, box))
                sameCount++;
            else
                break;

            i++;
        }

        return sameCount;
    }

    #endregion

    #region BoxesOfSameTypeDown

    public int BoxesOfSameTypeDown(IBox box)
    {
        int sameCount = 0;
        if (box == null)
            throw new Exception("Box cannot be null");

        int i = box.LogicalY;

        while (i > MinimumLogicalY)
        {
            if (IsEmptyCell(box.LogicalX, i - 1))     //tested box cannot be null or empty
                break;

            IBox boxToCheck = GetBox(box.LogicalX, i-1);

            //if (boxToCheck.GetType() == box.GetType())
            if (AreBoxesTheSameType(boxToCheck, box))
                sameCount++;
            else
                break;

            i--;
        }

        return sameCount;
    }

    #endregion

    #region BoxesOfSameTypeUp

    public int BoxesOfSameTypeUp(IBox box)
    {
        int sameCount = 0;
        if (box == null)
            throw new Exception("Box cannot be null");

        int i = box.LogicalY;

        while (i < MaximumLogicalY)
        {
            if (IsEmptyCell(box.LogicalX, i + 1))     //tested box cannot be null or empty
                break;

            IBox boxToCheck = GetBox(box.LogicalX, i+1);

            //if (boxToCheck.GetType() == box.GetType())
            if (AreBoxesTheSameType(boxToCheck, box))
                sameCount++;
            else
                break;

            i++;
        }

        return sameCount;
    }

    #endregion

    #region CollapseMatrix

    public void CollapseMatrix()
    {
        for (int i=MinimumLogicalX; i<=MaximumLogicalX; i++)
        {
            for (int j=MinimumLogicalY; j<=MaximumLogicalY; j++)
            {
                if(IsEmptyCell(i,j))
                {
                    int k = j + 1;
                    while(k<=MaximumLogicalY) 
                    {
                        if (IsEmptyCell(i, k))
                            k++;
                        else
                        {
                            MoveBox(i, k, 0, j-k);  //is MoveBox function working properly??
                            break;                  //it's enough to find first candidate to move box down
                        }
                    }

                }
            }
        }
    }

    #endregion

    #region FillUpMatrixWithNewBoxes

    public void FillUpMatrixWithNewBoxes()
    {
        for (int i = MinimumLogicalX; i <= MaximumLogicalX; i++)
        {
            //check how many empty cells is in a column
            int emptyCells = 0;
            for (int j = MaximumLogicalY; j >= MinimumLogicalY; j--)
            {
                if (IsEmptyCell(i, j))
                    emptyCells++;
            }

            //register new boxes in a column
            //separated loop to handle special effects of falling boxes
            for (int j = MaximumLogicalY-emptyCells; j < MaximumLogicalY; j++)
            {
                IBox newBox = MatrixCreator.GetMatchingBox(this, i, j+1);
                if (newBox != null)
                    this.RegisterBox(newBox);
            }

        }
    }

    #endregion

    #region GetAllBoxesInTheSameRow

    public IList<IBox> GetAllBoxesInTheSameRow(IBox box)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box == null)
            throw new Exception("Box cannot be null");

        var boxesInRow = _boxes.Where(b => b.LogicalY == box.LogicalY).ToList();

        return boxesInRow;
    }

    #endregion GetAllBoxesInTheSameRow

    #region GetAllBoxesInTheSameColumn

    public IList<IBox> GetAllBoxesInTheSameColumn(IBox box)
    {
        if (_boxes == null)
            throw new Exception("Matrix was not initialized");

        if (box == null)
            throw new Exception("Box cannot be null");

        var boxesInColumn = _boxes.Where(b => b.LogicalX == box.LogicalX).ToList();

        return boxesInColumn;
    }

    #endregion GetAllBoxesInTheSameColumn
}
