﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MatrixLevelOne : MatrixBase
{
    public MatrixLevelOne(int logicalWidth, int logicalHeight) : base(logicalWidth, logicalHeight)
    {

    }

    public override void GenerateMatrix()
    {
        for (int i = MinimumLogicalX; i <= MaximumLogicalX; i++)
        {
            for (int j = MinimumLogicalY; j <= MaximumLogicalY; j++)
            {
                IBox newBox = MatrixCreator.GetMatchingBox(this, i, j);
                if (newBox != null)
                    this.RegisterBox(newBox);
            }
        }
    }

    public override void RegisterMatrixConditions()
    {
        MatrixCreator.ClearConditions();
        MatrixCreator.RegisterCondition<NeighbourCondition>();
        MatrixCreator.RegisterCondition<SixBoxesTypesInMatrixCondition>();
    }

    public override void RegisterAvailableHits()
    {
        HitManager.ClearHitTypes();
        HitManager.RegisterHitType(HitTypeEnum.TwoElementsHit);
        HitManager.RegisterHitType(HitTypeEnum.ThreeElementsHit);
        HitManager.RegisterHitType(HitTypeEnum.FourElementsHit);
        HitManager.RegisterHitType(HitTypeEnum.FiveElementsHit);
        HitManager.RegisterHitType(HitTypeEnum.LHit);
    }
}
