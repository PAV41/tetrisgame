﻿
using System.Collections;
using System.Collections.Generic;

public interface IMatrix
{
    int LogicalWidth { get; set; }

    int LogicalHeight { get; set; }

    int MinimumLogicalX { get; }

    int MaximumLogicalX { get; }

    int MinimumLogicalY { get; }

    int MaximumLogicalY { get; }

    ///<summary>
    ///Registers matrix's creation rules.
    ///</summary>
    void RegisterMatrixConditions();

    ///<summary>
    ///Returns all boxes registered in the matrix.
    ///</summary>
    IList<IBox> GetAllBoxes();

    ///<summary>
    ///Returns box at position X,Y if exists.
    ///</summary>
    IBox GetBox(int logicalPosX, int logicalPosY);

    ///<summary>
    ///Returns boxes which are currently selected.
    ///</summary>
    IList<IBox> GetSelectedBoxes();

    ///<summary>
    ///Returns all active neighbors of the box.
    ///</summary>
    IList<IBox> GetBoxesNeighbours(IBox box);

    ///<summary>
    ///Checks if two boxes are neighbors.
    ///</summary>
    bool AreBoxesNeighbours(IBox box1, IBox box2);

    ///<summary>
    ///Checks if two boxes have the same type (as example if two boxes are blue).
    ///</summary>
    bool AreBoxesTheSameType(IBox box1, IBox box2);

    ///<summary>
    ///Adds new box to the matrix and creates physical game object from assigned prefab.
    ///</summary>
    void RegisterBox(IBox box);

    ///<summary>
    ///Removes box from the matrix and destroys physical game object.
    ///</summary>
    void UnregisterBox(IBox box);

    ///<summary>
    ///Replaces two boxes in the matrix.
    ///</summary>
    void ReplaceBoxes(IBox box1, IBox box2);

    ///<summary>
    ///Moves box from position X,Y to another position X,Y. Last two parameters are movement parameters.
    ///</summary>
    void MoveBox(int logicalX, int logicalY, int moveLogicalX, int moveLogicalY);

    ///<summary>
    ///Removes all boxes from the matrix and destroys physicals game objects.
    ///</summary>
    void ClearMatrix();

    ///<summary>
    ///Checks if cell at position X,Y is empty.
    ///</summary>
    bool IsEmptyCell(int logicalPosX, int logicalPosY);

    ///<summary>
    ///Checks if cell above the box is empty.
    ///</summary>
    bool IsUpCellEmpty(IBox box);

    ///<summary>
    ///Checks if cell below the box is empty.
    ///</summary>
    bool IsBelowCellEmpty(IBox box);

    ///<summary>
    ///Checks if cell left of the box is empty.
    ///</summary>
    bool IsLeftCellEmpty(IBox box);

    ///<summary>
    ///Checks if right cell of the box is empty.
    ///</summary>
    bool IsRightCellEmpty(IBox box);

    ///<summary>
    ///Checks if first box is above second box.
    ///</summary>
    bool IsBoxAbove(IBox box1, IBox box2);

    ///<summary>
    ///Checks if first box is on the right side of second box.
    ///</summary>
    bool IsBoxOnTheRight(IBox box1, IBox box2);

    ///<summary>
    ///Checks if first box is on the left side of second box.
    ///</summary>
    bool IsBoxOnTheLeft(IBox box1, IBox box2);

    ///<summary>
    ///Checks if first box is below second box.
    ///</summary>
    bool IsBoxBelow(IBox box1, IBox box2);

    ///<summary>
    ///Checks if two boxes are in the same matrix's column.
    ///</summary>
    bool AreBoxesInTheSameColumn(IBox box1, IBox box2);

    ///<summary>
    ///Checks if two boxes are in the same matrix's row.
    ///</summary>
    bool AreBoxesInTheSameRow(IBox box1, IBox box2);

    ///<summary>
    ///Moves boxes which are in the same column above removed box one cell down.
    ///</summary>
    void UpdateMatrixAfterRemovement(IBox removedBox);

    ///<summary>
    ///Removes recursively boxes which have the same type as input box type. Function stops on first box which has different type. Position of input box is start point.
    ///</summary>
    void RemoveTheSameBoxesRecursively(IBox box);

    ///<summary>
    ///Creates matrix for each level.
    ///</summary>
    void GenerateMatrix();

    ///<summary>
    ///Returns list of boxes of the same type which are neghbours in a column including the given box.
    ///</summary>
    IList<IBox> GetBoxesOfSameTypeInAColumn(IBox box);

    ///<summary>
    ///Returns list of boxes of the same type which are neghbours in a row including the given box.
    ///</summary>
    IList<IBox> GetBoxesOfSameTypeInARow(IBox box);

    ///<summary>
    ///Checks how many boxes of the same type is placed to the left from the given box (without it).
    ///</summary>
    int BoxesOfSameTypeToTheLeft(IBox box);

    ///<summary>
    ///Checks how many boxes of the same type is placed to the right from the given box (without it).
    ///</summary>
    int BoxesOfSameTypeToTheRight(IBox box);

    ///<summary>
    ///Checks how many boxes of the same type is placed above the given box (without it).
    ///</summary>
    int BoxesOfSameTypeUp(IBox box);

    ///<summary>
    ///Checks how many boxes of the same type is placed below the given box (without it).
    ///</summary>
    int BoxesOfSameTypeDown(IBox box);

    ///<summary>
    ///Collapses boxes down after performing a step (at least 3 boxes were found and destroyed).
    ///</summary>
    void CollapseMatrix();

    ///<summary>
    ///Fills up matrix with new boxes from the top, should be executed after CollapseMatrix(). Returns list of new boxes.
    ///</summary>
    void FillUpMatrixWithNewBoxes();

    ///<summary>
    ///Returns list of boxes in the same row as given box.
    ///</summary>
    IList<IBox> GetAllBoxesInTheSameRow(IBox box);

    ///<summary>
    ///Returns list of boxes in the same column as given box.
    ///</summary>
    IList<IBox> GetAllBoxesInTheSameColumn(IBox box);
}