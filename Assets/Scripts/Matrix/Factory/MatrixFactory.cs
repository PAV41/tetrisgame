﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

///<summary>
///Helper static class (factory) responsible for matrixes instances creation.
///</summary>
public static class MatrixFactory {

    ///<summary>
	///Creates various matrixes instances.
	///</summary>
	public static IMatrix CreateMatrix(MatrixTypeEnum type, int logicalWidth, int logicalHeight)
    {
        IMatrix matrix = null;
        switch (type)
        {
            case MatrixTypeEnum.MatrixLevelOne:
                matrix = new MatrixLevelOne(logicalWidth, logicalHeight);
                break;
            default:
                break;
        }
        return matrix;
    }
}
